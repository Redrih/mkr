<?php

require_once 'Good.php';
class Magaz
{
    private $goods;

    public function countGoods()
    {
        return count($this->goods);
    }

    public function countUnder18Goods()
    {
        $under18Goods = [];
        foreach ($this->goods as $good) {
            if ($good->getUnder18())
            {
                $under18Goods[] = $good;
            }
        }
        return count($under18Goods);
    }

    /**
     * Magaz constructor.
     * @param $goods
     */
    public function __construct($goods)
    {
        $this->goods = $goods;
    }


}