<?php
require_once 'Good.php';
require_once 'Magaz.php';

$goods = [];
$goods[] = new Good('banan',39,true);
$goods[] = new Good('sizhka',70,false);
$goods[] = new Good('pivko',40,false);
$goods[] = new Good('kepka', 499,true);

$magaz = new Magaz($goods);

echo $magaz->countGoods() . "\n";
echo $magaz->countUnder18Goods() . "\n";