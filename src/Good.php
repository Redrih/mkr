<?php


class Good
{
    private $goodName;
    private $cost;
    private $under18;

    /**
     * Good constructor.
     * @param $goodName
     * @param $cost
     * @param $adultOnly
     */
    public function __construct($goodName, $cost, $adultOnly)
    {
        $this->goodName = $goodName;
        $this->cost = $cost;
        $this->adultOnly = $adultOnly;
    }

    /**
     * @return mixed
     */
    public function getGoodName()
    {
        return $this->goodName;
    }

    /**
     * @param mixed $goodName
     */
    public function setGoodName($goodName)
    {
        $this->goodName = $goodName;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param mixed $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return mixed
     */
    public function getUnder18()
    {
        return $this->adultOnly;
    }

    /**
     * @param mixed $adultOnly
     */
    public function setUnder18($adultOnly)
    {
        $this->adultOnly = $adultOnly;
    }


}